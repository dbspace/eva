package io.xxx.eva.robot.send.limit;

import io.xxx.eva.robot.Robot;
import io.xxx.eva.task.TaskMessage;

/**
 * 消息发送数量限流器
 * <p>
 * 控制机器人在单位时间内发送的消息数，防止微信被风控
 */
public interface MessageLimiter {

    void acquire(Robot robot, TaskMessage message);
}
