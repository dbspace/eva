package io.xxx.eva.robot.send;

import io.xxx.eva.receiver.Receiver;
import io.xxx.eva.task.TaskMessage;

/**
 * 将消息发送给接收者
 * <p>
 * 有两种实现方式：
 * 1. 模拟手机上的人工操作
 * 2. 模拟iPad上的接口调用
 */
public interface Sender {

    void send(Receiver receiver, TaskMessage message);
}
