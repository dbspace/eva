package io.xxx.eva.robot.send.limit;

import io.xxx.eva.task.MessageLimitRuleRepository;
import io.xxx.eva.task.PushRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractMessageLimiter implements MessageLimiter {

    @Autowired
    protected MessageLimitRuleRepository limitRuleRepository;

    @Autowired
    protected PushRecordRepository pushRecordRepository;
}
