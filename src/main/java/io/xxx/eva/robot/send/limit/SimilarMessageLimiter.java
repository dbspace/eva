package io.xxx.eva.robot.send.limit;

import io.xxx.eva.robot.Robot;
import io.xxx.eva.task.TaskMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 相似消息条数限制
 */
@Slf4j
@Component
public class SimilarMessageLimiter extends AbstractMessageLimiter {

    @Override
    public void acquire(Robot robot, TaskMessage message) {
        if (isSimilar(robot, message)) {
            System.out.println(123);
        }
    }

    private boolean isSimilar(Robot robot, TaskMessage message) {
        return false;
    }
}
