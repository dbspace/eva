package io.xxx.eva.robot.send;

public record SendResult(
        int code,

        String message
) {

    boolean isSuccess() {
        return code == 0;
    }
}
