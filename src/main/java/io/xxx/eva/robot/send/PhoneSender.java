package io.xxx.eva.robot.send;

import com.alibaba.fastjson.JSON;
import io.xxx.eva.receiver.Receiver;
import io.xxx.eva.robot.device.DeviceHandler;
import io.xxx.eva.task.TaskMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Slf4j
@Order(2)
@Component
public class PhoneSender implements Sender {

    private final DeviceHandler deviceHandler;

    public PhoneSender(DeviceHandler deviceHandler) {
        this.deviceHandler = deviceHandler;
    }

    @Override
    public void send(Receiver receiver, TaskMessage message) {
        log.debug("发送消息[receiver:{},message:{}]", JSON.toJSONString(receiver), JSON.toJSONString(message));
        deviceHandler.sendMessage(receiver.id(), message);
    }
}
