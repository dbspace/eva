package io.xxx.eva.robot;

import io.xxx.eva.seller.Seller;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
public class Robot {

    @Id
    @Column(length = 20)
    private String id;

    @Column(length = 50)
    private String name;

    private Boolean enabled;

    @ManyToMany(mappedBy = "robots")
    private List<Seller> sellers;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;
}
