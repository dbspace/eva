package io.xxx.eva.robot.device;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.netty.channel.*;
import io.xxx.eva.task.TaskMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

@Component
@ChannelHandler.Sharable
public class DeviceHandler extends SimpleChannelInboundHandler<String> {

    private final Map<String, Channel> channels = new HashMap<>();

    @Autowired
    private DeviceRepository deviceRepository;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        Channel channel = ctx.channel();
        channels.put(msg, channel);
        Optional<Device> deviceOptional = deviceRepository.findById(msg);
        if (deviceOptional.isEmpty()) {
            Device device = new Device();
            device.setId(msg);
            deviceRepository.save(device);
        }
    }

    public void sendMessage(String deviceId, TaskMessage message) {
        Channel channel = channels.get(deviceId);
        channel.writeAndFlush(JSON.toJSONString(message));
    }
}
