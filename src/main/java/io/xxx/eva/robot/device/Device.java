package io.xxx.eva.robot.device;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Device {

    @Id
    private String id;

    private String phone;

    private String channelId;
}
