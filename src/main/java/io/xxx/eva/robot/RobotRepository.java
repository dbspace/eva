package io.xxx.eva.robot;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RobotRepository extends JpaRepository<Robot, String> {

    List<Robot> findByNameLike(String name);
}
