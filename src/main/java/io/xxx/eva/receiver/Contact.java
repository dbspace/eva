package io.xxx.eva.receiver;

import io.xxx.eva.robot.Robot;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.Data;

import java.util.List;

@Data
@Entity
public class Contact implements Receiver {

    @Id
    private Long id;

    @Column(length = 64)
    private String nickName;

    @Column(length = 20)
    private String phone;

    @ManyToMany
    private List<Robot> robots;

    @Override
    public Type type() {
        return Type.CONTACT;
    }

    @Override
    public String id() {
        return id.toString();
    }

    @Override
    public String name() {
        return nickName;
    }

    @Override
    public List<Robot> robots() {
        return robots;
    }
}
