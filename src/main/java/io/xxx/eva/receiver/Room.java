package io.xxx.eva.receiver;

import io.xxx.eva.robot.Robot;
import io.xxx.eva.seller.Seller;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import lombok.Data;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Data
@Entity
public class Room implements Receiver {

    @Id
    private String id;

    private String topic;

    @ManyToMany
    private List<Robot> robots;

    @ManyToOne
    private Seller seller;

    @Override
    public Type type() {
        return Type.ROOM;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return topic;
    }

    @Override
    public List<Robot> robots() {
        return robots;
    }
}
