package io.xxx.eva.receiver;

import com.alibaba.fastjson.annotation.JSONField;
import io.xxx.eva.robot.Robot;

import java.util.List;

public interface Receiver {

    Type type();

    String id();

    String name();

    @JSONField(serialize = false)
    List<Robot> robots();

    enum Type {
        CONTACT,

        ROOM
    }
}
