package io.xxx.eva.task;

import io.xxx.eva.robot.Robot;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Entity
public class PushRecord {

    @Id
    private Long id;

    @ManyToOne
    private Robot robot;

    @ManyToOne
    private Task task;

    @ManyToOne
    private TaskMessage message;

    private LocalDateTime pushTime;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;
}
