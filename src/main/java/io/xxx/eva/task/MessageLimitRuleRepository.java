package io.xxx.eva.task;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageLimitRuleRepository extends JpaRepository<MessageLimitRule, Long> {
}
