package io.xxx.eva.task;

import io.xxx.eva.receiver.Contact;
import io.xxx.eva.receiver.Receiver;
import io.xxx.eva.receiver.Room;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.*;
import org.hibernate.type.SqlTypes;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@Entity
public class Task {

    @Id
    private Long id;

    private String name;

    @Enumerated
    private ExecutionMode executionMode;

    @Enumerated
    private SelectMode selectMode;

    @Column(columnDefinition = "jsonb")
    @JdbcTypeCode(SqlTypes.JSON)
    private Map<String, String> selectContent;

    @Enumerated
    private SendMode sendMode;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private Status status;

    private Integer retryTimes;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;

    @OneToMany(mappedBy = "task")
    private List<TaskMessage> messages;

    /**
     * 返回接收人信息需要控制数量
     */
    @ManyToAny
    @AnyDiscriminator(DiscriminatorType.INTEGER)
    @AnyDiscriminatorValue(discriminator = "0", entity = Room.class)
    @AnyDiscriminatorValue(discriminator = "1", entity = Contact.class)
    @AnyKeyJavaClass(Long.class)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @Column(name = "receiver_type")
    @JoinTable(name = "task_receiver",
            joinColumns = @JoinColumn(name = "task_id"),
            inverseJoinColumns = @JoinColumn(name = "receiver_id")
    )
    private List<? extends Receiver> receivers;

    public enum ExecutionMode {
        ONCE,

        CYCLE
    }

    public enum SelectMode {
        DATABASE,

        FILE
    }

    public enum SendMode {
        IPAD,
        PHONE
    }

    public enum Status {
        NEW,
        RUNNING,
        CANCELED,
        COMPLETED
    }
}
