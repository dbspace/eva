package io.xxx.eva.task.select;

import io.xxx.eva.receiver.Room;
import io.xxx.eva.receiver.RoomRepository;
import io.xxx.eva.task.Task;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

@Slf4j
@Component
public class DatabaseReceiverSelector implements ReceiverSelector {

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public Flux<Room> select(Task task) {
        return Flux.create(emitter -> {
            for (Room room : roomRepository.findAll()) {
                emitter.next(room);
            }
            emitter.complete();
        });
    }
}
