package io.xxx.eva.task.select;

import io.xxx.eva.receiver.Room;
import io.xxx.eva.task.Task;
import reactor.core.publisher.Flux;

public interface ReceiverSelector {

    Flux<Room> select(Task task);
}
