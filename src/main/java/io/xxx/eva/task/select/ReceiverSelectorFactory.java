package io.xxx.eva.task.select;

import io.xxx.eva.task.Task;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class ReceiverSelectorFactory {

    @Resource
    private DatabaseReceiverSelector databaseReceiverSelector;

    @Resource
    private FileReceiverSelector fileReceiverSelector;

    public ReceiverSelector getReceiverSelector(Task.SelectMode selectMode) {
        return switch (selectMode) {
            case DATABASE -> databaseReceiverSelector;
            case FILE -> fileReceiverSelector;
        };
    }
}
