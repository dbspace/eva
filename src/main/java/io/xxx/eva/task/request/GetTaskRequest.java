package io.xxx.eva.task.request;

import io.xxx.eva.task.Task;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class GetTaskRequest {

    private String name;

    private Task.ExecutionMode executionMode;

    private Task.SelectMode selectMode;

    private Task.SendMode sendMode;

    private Task.Status status;
}
