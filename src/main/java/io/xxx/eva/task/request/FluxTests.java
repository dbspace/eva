package io.xxx.eva.task.request;

import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class FluxTests {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
//        Flux.range(1, 100)
////                .delayElements(Duration.ofMillis(10))
////                .limitRate(1)
//                .onBackpressureBuffer(1)
////                .delayElements(Duration.ofMillis(100))
//                .subscribe(
//                        item -> {
//                            System.out.println(item);
//                            try {
//                                TimeUnit.SECONDS.sleep(1);
//                            } catch (InterruptedException e) {
//                                throw new RuntimeException(e);
//                            }
//                        },
//                        ex -> System.out.println("onError: " + ex),
//                        () -> {
//                            System.out.println("onComplete");
//                            latch.countDown();
//                        }
//                );
        Flux.create(emitter -> {
                    for (int i = 0; i < 1000; i++) {
                        emitter.next(i);
                    }
                })
                .onBackpressureBuffer(1)
                .delayElements(Duration.ofMillis(100))
                .subscribe(
                        item -> {
                            System.out.println(item);
                            try {
                                TimeUnit.SECONDS.sleep(1);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        },
                        ex -> System.out.println("onError: " + ex),
                        () -> {
                            System.out.println("onComplete");
                            latch.countDown();
                        }
                );
        latch.await();
    }
}
