package io.xxx.eva.task.request;

import io.xxx.eva.task.TaskRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AddTaskRequest extends TaskRequest {
}
