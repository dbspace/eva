package io.xxx.eva.task;

import io.xxx.eva.robot.Robot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Map;

public interface PushRecordRepository extends JpaRepository<PushRecord, Long> {

//    @Query(nativeQuery = true, value = "select count(*), max(createdTime) from PushRecord pr where pr.")
//    Map<String, Object> findByRobotAndTask(Robot robot, Task task);
}
