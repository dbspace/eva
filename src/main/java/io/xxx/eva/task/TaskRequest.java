package io.xxx.eva.task;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class TaskRequest {

    private String name;

    private Task.ExecutionMode executionMode;

    private Task.SelectMode selectMode;

    private Task.SendMode sendMode;

    private List<TaskMessage> messages;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private Task.Status status;

    private Integer retryTimes;
}
