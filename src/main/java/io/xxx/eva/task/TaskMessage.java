package io.xxx.eva.task;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.Map;

/**
 * 任务待推送消息
 */
@Data
@Entity
public class TaskMessage {

    @Id
    private Long id;

    @ManyToOne
    private Task task;

    @Column
    private Integer sn;

    @Enumerated
    private Message.Type type;

    /**
     * 和message不可以同时为空
     */
    @Column(columnDefinition = "jsonb")
    @JdbcTypeCode(SqlTypes.JSON)
    private Map<String, Object> payload;

    @ManyToOne
    private Message message;
}
