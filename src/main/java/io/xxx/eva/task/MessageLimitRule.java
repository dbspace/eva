package io.xxx.eva.task;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class MessageLimitRule {

    @Id
    private Long id;

    private String name;

    private Integer quantity;
}
