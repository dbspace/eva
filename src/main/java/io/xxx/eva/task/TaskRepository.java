package io.xxx.eva.task;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Modifying
    @Query(value = "update Task t set t.status = ?2 where t.id = ?1", nativeQuery = true)
    void updateStatus(long taskId, Task.Status status);
}
