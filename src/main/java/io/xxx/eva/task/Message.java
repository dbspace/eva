package io.xxx.eva.task;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.Data;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.Map;

/**
 * 素材库中的消息
 */
@Data
@Entity
public class Message {

    @Id
    private Long id;

    @Enumerated
    private Type type;

    @Column(columnDefinition = "jsonb")
    @JdbcTypeCode(SqlTypes.JSON)
    private Map<String, Object> payload;

    public enum Type {

        TEXT,

        IMAGE,

        VIDEO,

        MINI_PROGRAM
    }
}
