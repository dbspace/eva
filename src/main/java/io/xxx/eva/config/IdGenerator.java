package io.xxx.eva.config;

import jakarta.annotation.Resource;
import org.apache.logging.log4j.util.Strings;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class IdGenerator {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyMMdd");

    @Resource
    private RedissonClient redissonClient;

    public Long nextId() {
        String prefix = LocalDate.now().format(FORMATTER);
        RAtomicLong taskId = redissonClient.getAtomicLong("taskId");
        String suffix = Strings.repeat("0", 9 - String.valueOf(taskId.incrementAndGet()).length());
        return Long.valueOf(prefix + suffix);
    }
}
