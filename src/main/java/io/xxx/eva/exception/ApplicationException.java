package io.xxx.eva.exception;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ApplicationException extends RuntimeException {

    private final int code;

    private final String message;

    public ApplicationException(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
