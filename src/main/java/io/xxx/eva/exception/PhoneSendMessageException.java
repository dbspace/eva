package io.xxx.eva.exception;

public class PhoneSendMessageException extends ApplicationException {

    public PhoneSendMessageException() {
        super(1000001, "手机发送消息失败");
    }
}
