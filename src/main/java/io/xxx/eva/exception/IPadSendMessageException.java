package io.xxx.eva.exception;

public class IPadSendMessageException extends ApplicationException {

    public IPadSendMessageException() {
        super(1000002, "iPad发送消息失败");
    }
}
