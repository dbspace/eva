package io.xxx.eva.seller;

import io.xxx.eva.robot.Robot;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
public class Seller {

    @Id
    private Long id;

    @ManyToMany
    private List<Robot> robots;

    private LocalDateTime createdTime;

    private LocalDateTime updatedTime;
}
